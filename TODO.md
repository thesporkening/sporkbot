`Key
    x => Done
    w => WIP
    () => version
`
Plans 1.0:
    
    Features:
    [w] Reaction roles
        
    Backend:
    [x] MongoDB
    [] Dashboard
    
    Profiles:
    [w] XP/Levels
        [w] Gain XP from voice channels
    [w] Coins
    
    Fun:
    [] Shop system (v2)
    [] Karma System
    [] Meme System
        [] React for karma
        [] Re-Post for user
        
    
    Moderation:
    [] Add permissions checker for commands
    [w] Create custom channels
        [] Add limit of channels user can make