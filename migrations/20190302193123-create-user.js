'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('Users', {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER,
			},
			user_id: {
				allowNull: false,
				type: Sequelize.STRING,
			},
			server_id: {
				allowNull: false,
				type: Sequelize.STRING,
			},
			xp: {
				type: Sequelize.INTEGER,
				default: 0,
			},
			coins: {
				type: Sequelize.INTEGER,
				default: 0,
			},
			createdAt: {
				allowNull: false,
				type: Sequelize.DATE,
			},
			updatedAt: {
				allowNull: false,
				type: Sequelize.DATE,
			},
		});
	},
	down: (queryInterface) => {
		return queryInterface.dropTable('Users');
	},
};