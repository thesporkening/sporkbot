#SporkBOT
![build status](https://img.shields.io/gitlab/pipeline/thesporkening/sporkbot/master.svg)
![Discord Link](https://img.shields.io/discord/128797258287153152.svg?label=Join%20Us%21)

This is the discord bot you need to have!

#Features
* Meme Channel
* Level System(Gain XP from Voice and Text)
* Top Users Command

#Libs
* Node-Fetch
* Discordjs
* yt-dl

